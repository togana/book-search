import { observable, computed, action } from 'mobx';
import qs from 'qs';

const booksVolumesListAPI = 'https://www.googleapis.com/books/v1/volumes';
const maxResults = 40;

export default class Books {
  @observable.shallow list = [];
  @observable totalItems = 0;
  @observable from = maxResults;

  @computed get listInfo() {
    return this.list.map(o => ({
      id: o.id,
      title: o.volumeInfo.title,
      subtitle: o.volumeInfo.subtitle,
      description: o.volumeInfo.description,
    }));
  }

  @action async fetchData(searchText) {
    if (searchText === null || searchText === '') return;
    const query = {
      q: searchText,
      maxResults,
    };
    const queryString = qs.stringify(query);

    const json = await fetch(`${booksVolumesListAPI}?${queryString}`)
      .then(v => v.json())
      .catch(e => window.console.error(e));
    if (!json) return;
    this.totalItems = json.totalItems;
    this.list = json.items || [];
  }
}
