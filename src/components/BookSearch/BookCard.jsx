import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader, CardContent, Typography } from 'material-ui';

export default class BookCard extends PureComponent {
  static propTypes = {
    info: PropTypes.object.isRequired,
  };

  render() {
    const { info } = this.props;
    return (
      <Card>
        <CardHeader
          title={info.title}
          subheader={info.subtitle}
        />
        <CardContent>
          <Typography>
            {info.description}
          </Typography>
        </CardContent>
      </Card>
    );
  }
}
