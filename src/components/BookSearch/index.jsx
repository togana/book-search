import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { Input, FormControl, InputLabel } from 'material-ui';
import BookCard from './BookCard';

@inject('book')
@observer
export default class BookSearch extends Component {
  static propTypes = {
    book: PropTypes.object.isRequired,
  };

  render() {
    const { book } = this.props;

    return (
      <div>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            this.props.book.fetchData(document.getElementById('search-field').value);
          }}
        >
          <FormControl>
            <InputLabel htmlFor="search-field">検索</InputLabel>
            <Input
              id="search-field"
              autoFocus
            />
          </FormControl>
        </form>
        {book.totalItems ? `${book.totalItems}件中1〜${book.totalItems < 40 ? book.totalItems : book.from}件表示` : null}
        <div>
          {book.listInfo.map(o => <BookCard key={o.id} info={o} />)}
        </div>
      </div>
    );
  }
}
