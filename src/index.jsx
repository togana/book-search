import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'mobx-react';
import Books from './stores/Books';
import BookSearch from './components/BookSearch';

const stores = {
  book: new Books(),
};

render(
  <Provider {...stores}>
    <BookSearch />
  </Provider>,
  document.getElementById('root'),
);
